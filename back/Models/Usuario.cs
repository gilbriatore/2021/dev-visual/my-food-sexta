
using System.Text.Json.Serialization;

namespace MyFood.Models {

  public class Usuario {

    public int Id { get; set; }
    public string Login { get; set; }
    public string Papel { get; set; }

    [JsonIgnore]
    public string Senha { get; set; }
  }
}

//dotnet-aspnet-codegenerator controller -name UsuarioController -m Usuario -dc MyFoodContext -api --relativeFolderPath Controllers