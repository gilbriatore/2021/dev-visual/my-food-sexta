using System;
using System.Collections.Generic;

namespace MyFood.Models {

  public class Pedido {

    public int Id { get; set; }
    public DateTime Data { get; set; }
    public Status Status { get; set; }
    public Cliente Cliente { get; set; }
    public List<Produto> Itens { get; set; }

  }
}

//dotnet-aspnet-codegenerator controller -name PedidoController -m Pedido -dc MyFoodContext -api --relativeFolderPath Controllers