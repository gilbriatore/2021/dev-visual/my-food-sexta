
namespace MyFood.Models {

  public class Cliente : Usuario {

    public string nome { get; set; } 
    public Loja Loja { get; set; }

  }

}

//dotnet-aspnet-codegenerator controller -name ClienteController -m Cliente -dc MyFoodContext -api --relativeFolderPath Controllers