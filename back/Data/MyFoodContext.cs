using Microsoft.EntityFrameworkCore;

using MyFood.Models;

namespace MyFood.Data {

  public class MyFoodContext : DbContext {

    public DbSet<Categoria> Categorias { get; set; }
    public DbSet<Cliente> Clientes { get; set; }
    public DbSet<Loja> Lojas { get; set; }
    public DbSet<Pedido> Pedidos { get; set; }
    public DbSet<Produto> Produtos { get; set; }
    public DbSet<Status> Status { get; set; }
    public DbSet<Usuario> Usuarios { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder builder)
    {
      builder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=MyFood;Integrated Security=True");
    }

  }
}