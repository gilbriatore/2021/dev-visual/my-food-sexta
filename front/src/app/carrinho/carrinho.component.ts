import { Component, OnInit } from '@angular/core';
import { CadastroService } from '../services/cadastro.service';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.component.html',
  styleUrls: ['./carrinho.component.css']
})
export class CarrinhoComponent implements OnInit {

  constructor(private cadastro: CadastroService) { }

  ngOnInit(): void {
  }

  executar(){
    //this.cadastro.log();
  }

}
