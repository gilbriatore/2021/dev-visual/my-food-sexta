import { Component, OnInit } from '@angular/core';
import { Status } from '../models/status.model';
import { CadastroService } from '../services/cadastro.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  constructor(private cadastro: CadastroService) { 
  }

  ngOnInit(): void {
    this.listarStatus();
  }

  status : Status = {} as Status;
  statusFront : Status[] = [];
  displayedColumns: string[] = ['id', 'nome', 'acoes'];

  salvarStatus(){
    this.cadastro.salvarStatus(this.status).subscribe(() => {
      this.listarStatus();
    });
  }

  buscarStatus(id : number){
    this.cadastro.buscarStatus(id).subscribe(statusBack => {
      this.status = statusBack;
    });
  }

  excluirStatus(id : number){
    this.cadastro.excluirStatus(id).subscribe(() => {
      this.listarStatus();
    })
  }

  listarStatus(){
    this.cadastro.listarStatus().subscribe(statusBack => {
      this.statusFront = statusBack;
    });
  }

}
